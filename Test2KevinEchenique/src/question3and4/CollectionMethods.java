package question3and4;

import java.util.Collection;
import java.util.ArrayList;

public class CollectionMethods {
	
	public static Collection<Planet> getLargerThan(Collection<Planet> planets, double size){
		
		Collection<Planet> manyPlanets = new ArrayList<Planet>();
		for(Planet currentPlanet : planets) {
			if(currentPlanet.getRadius() >= size) {
				manyPlanets.add(currentPlanet);
			}
		}
		return manyPlanets;
		
	}

}
