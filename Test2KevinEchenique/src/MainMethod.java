
public class MainMethod {
	public static void main(String[] args) {
		
		Employee[] workers = new Employee[5];
		workers[0] = new SalariedEmployee(80000);
		workers[1] = new HourlyEmployee(40, 15);
		workers[2] = new UnionizedHourlyEmployee(50, 20, 40, 1.5);
		workers[3] = new SalariedEmployee(150000);
		workers[4] = new HourlyEmployee(35, 14);
		
		PayrollManagement pay = new PayrollManagement();
		double totalExpenses = pay.getTotalExpenses(workers);
		
		System.out.println(totalExpenses);
		
	}

}
