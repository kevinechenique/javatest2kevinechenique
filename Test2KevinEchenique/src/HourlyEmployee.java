
public class HourlyEmployee implements Employee{
	
	private double weeklyHoursWorked;//hours worked in a week
	private double hourlyPay;
	
	public HourlyEmployee(double weeklyHoursWorked, double hourlyPay) {
		this.weeklyHoursWorked = weeklyHoursWorked;
		this.hourlyPay = hourlyPay;
	}
	
	/**
	 * Gives the weekly pay of an employee based on the number of hours worked
	 */
	public double getWeeklyPay() {
		double salaryPerWeek = this.weeklyHoursWorked * this.hourlyPay;
		
		return salaryPerWeek;
	}

	public double getWeeklyHoursWorked() {
		return this.weeklyHoursWorked;
	}
	public double getHourlyPay() {
		return this.hourlyPay;
	}
}
