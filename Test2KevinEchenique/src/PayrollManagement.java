
public class PayrollManagement {
	
	/**
	 * Get total weekly expenses
	 */
	public double getTotalExpenses(Employee[] employees) {
		double weeklyPay = 0;
		for(int i=0; i<employees.length; i++) {
			weeklyPay += employees[i].getWeeklyPay();//the way getWeeklyPay() method will work depends on the type of object created (ideally a UnionizedHourlyEmployee object)
		}
		return weeklyPay;
	}

}
