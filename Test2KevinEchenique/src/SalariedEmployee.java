
public class SalariedEmployee implements Employee{
	
	private double yearlySalary;//yearly salary
	
	public SalariedEmployee(double yearlySalary) {
		this.yearlySalary = yearlySalary;
	}
	
	/**
	 * calculates weekly pay using the yearlySalry
	 */
	public double getWeeklyPay() {
		double salaryPerWeek = this.yearlySalary/52;
		return salaryPerWeek;
	}
	
	public double getYearlySalary() {
		return this.yearlySalary;
	}

}
