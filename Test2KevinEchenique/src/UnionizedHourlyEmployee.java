
public class UnionizedHourlyEmployee extends HourlyEmployee{
	
	private double maxHoursPerWeek;
	private double overtimeRate;
	
	public UnionizedHourlyEmployee(double weeklyHoursWorked, double hourlyPay, double maxHoursPerWeek, double overtimeRate) {
		super(weeklyHoursWorked, hourlyPay);
		this.maxHoursPerWeek = maxHoursPerWeek;
		this.overtimeRate = overtimeRate;
	}
	
	/**
	 * Gets total Weekly salary of an employee
	 */
	public double getWeeklyPay() {
		double weeklyHours = getWeeklyHoursWorked();
		double hourlyPay = getHourlyPay();
		double salary = 0;//stores the salary
		
		if(weeklyHours <= this.maxHoursPerWeek) {
			salary = weeklyHours * hourlyPay;
		}
		else if(weeklyHours > this.maxHoursPerWeek) {
			double normalPay = weeklyHours * hourlyPay;//Calculates the usual salary
			double numberOfExtraHours = weeklyHours - this.maxHoursPerWeek; //calculates the number of extra hours worked
			double extraPay = numberOfExtraHours * this.overtimeRate;//calculates the extra pay that the employee gets
			
			salary = normalPay + extraPay;//Total payment with overtime included
		}
		return salary;
	}
	

}
